angular.module('starter.services', [])

.factory('instagramService',['$http', function($http) {
    var factory = {}; // it is a factory object

    $http.get("data.json")
        .then(function(response) {
            factory.instagramData = response.data; // factory.instagramData is a container that stores response that is returned by http.get method
            console.log(response);
        });

    return factory; // factory returns only one object
}]);

