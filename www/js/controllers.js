angular.module('starter.controllers', [])

.controller('HomeCtrl', ['$scope', 'instagramService', '$timeout', '$state', '$rootScope', '$stateParams', function($scope, instagramService, $timeout, $state, $rootScope, $stateParams) {
    $scope.$state = $state;
    $scope.data={};
    $scope.posts = [];
    $scope.commentable = false;
    
    $timeout(function() {
       $scope.data = instagramService.instagramData;
        $scope.posts = instagramService.instagramData.instagramDataStructure[0].post;  
        console.log($scope.data); 
    }, 1000);
    
    $scope.like = function(index) {
        $scope.posts[index].liked = true;
        $scope.posts[index].likes.push({
            likeId : $scope.posts[index].likes.length + 1,
            "user" : "tchandani",
            "avatar" : "img/chandani.jpg",
            "date" : new Date(),
            "liked" : true
        });
        console.log($scope.posts[index].likes);
    };
    
    $scope.unlike = function(index) {
        $scope.posts[index].liked = false;
        $scope.posts[index].likes.pop();
    };
    
    $scope.likersFunction = function() {
        $state.go('tab.home.likers');
    };
    
    $rootScope.$ionicGoBack = function() {
        $state.go('tab.home');
    };
    
    $scope.comment = function(index) {
        for(var i = 0; i < $scope.posts.length; i ++) {
            $scope.posts[index].commentable = true;
            $scope.commentable = true;
        }
    };
    
    $scope.disableComment = function(id) {
        for(var i = 0; i < $scope.posts.length; i ++) {
            if($scope.posts[id-1].commentable == true) {
                $scope.posts[id - 1].commentable = false;
                $scope.commentable = false;
            }
        }
    };
}])

.controller('likersCtrl', ['$scope', '$state','$stateParams', 'instagramService','$timeout', function($scope, $state, $stateParams, instagramService, $timeout) {
    
    $scope.likesArray = [];
    $scope.posts = [];
    
    $timeout(function() {
        $scope.posts = instagramService.instagramData.instagramDataStructure[0].post; 
        init();
    }, 1000);
    
    function init() {
        for(var i = 0; i < $scope.posts.length; i++) {
            if($scope.posts[i].postId == $stateParams.postId) {
                for(var k = 0; k < $scope.posts[i].likes.length; k++) {
                    $scope.likesArray.push($scope.posts[i].likes[k]);
                }
            } 
        } 
    };
    
}])

.controller('commentsCtrl', ['$scope', '$state', '$stateParams', 'instagramService', '$timeout', function($scope, $state, $stateParams, instagramService, $timeout) {
                             
    $scope.commentsArray = [];
    $scope.posts = [];
    $scope.myComment= {
        text : ""
    };
    

    $timeout(function(){
        $scope.posts = instagramService.instagramData.instagramDataStructure[0].post;
        init();
    }, 1000);
                            
    function init() {
        for(var i = 0; i < $scope.posts.length; i++) {
            if($scope.posts[i].postId == $stateParams.postId) {
                $scope.commentsArray = $scope.posts[i].comments;
            }
        }
    };
    
    $scope.submit = function(keyEvent) {
        if(keyEvent.keyCode === 13) {
            $scope.posts[$stateParams.postId - 1].comments.push({
                "commentId" : $scope.posts[$stateParams.postId - 1].comments.length + 1 ,
                "user" : "tchandani",
                "avatar" : "img/chandani.jpg",
                "commentText" : $scope.myComment.text,
                "date" : new Date()
            })
            $scope.myComment.text = "";
        }
    } 
}])

.controller('UploadController', function ($scope){
    var imageUpload = new ImageUpload();
    $scope.file = {};
    $scope.upload = function() {
        imageUpload.push($scope.file, function(data){
            console.log('File uploaded Successfully', $scope.file, data);
            $scope.uploadUri = data.url;
            $scope.$digest();
        });
    };
})
